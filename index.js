'use strict';

let ui = module.exports = {
    Component: require('./src/Component.js'),
    Router: require('./src/Router.js')
};

// jQuery support
// TODO: Might be a good idea to make this section a bit more robust outside of common uses?
if(window.jQuery) {
    let common = require('./src/common.js');

    let $ = window.jQuery,
        $detach = jQuery.fn.detach,
        $remove = jQuery.fn.remove,
        $text = jQuery.fn.text,
        $append = jQuery.fn.append,
		$prepend = jQuery.fn.prepend,
        $before = jQuery.fn.before,
        $after = jQuery.fn.after,
        $empty = jQuery.fn.empty,
        $clone = jQuery.fn.clone,
        $html = jQuery.fn.html,
        $replaceWith = jQuery.fn.replaceWith,
        $appendTo = jQuery.fn.appendTo,
        $prependTo = jQuery.fn.prependTo,
        $insertBefore = jQuery.fn.insertBefore,
        $insertAfter = jQuery.fn.insertAfter,
        $replaceAll = jQuery.fn.replaceAll;
    
    function jQueryPassThrough(method, sourceCallback, targetCallback) {
        if(method == null) {
            return null;
        }

        return function() {
            if(this.length && this[0] instanceof ui.Component) {
                return sourceCallback.apply(this, arguments);
            }
            else if(targetCallback && arguments.length) {
                if(arguments[0] instanceof ui.Component || (Array.isArray(arguments[0]) && arguments[0].filter((f) => f instanceof ui.Component).length)) {
                    return targetCallback.apply(this, arguments);
                }
            }
            return method.apply(this, arguments);
        };
    }

    function unsupportedMethod(message) {
        return () => {
            throw new Error(message);
        };
    }

    function applyAll(context, method, ...args) {
        if(Array.isArray(context) || context.length != null) {
            for(let i = 0; i < context.length; i++) {
                method.apply(context[i], args);
            }
        }
        else {
            method.apply(context, args);
        }
    }
    
    $.fn.html = jQueryPassThrough($html, unsupportedMethod('Components cannot be the source of $.html invocation'), function(components) {
        applyAll(components, ui.Component.prototype.attach, this);
        $empty.apply(this);
        applyAll(components, ui.Component.prototype._placeInParent, null, common.placementMethod.append);
        return this;
    });

    $.fn.text = jQueryPassThrough($text, function(selector) {
        if(selector != null) {
            throw new Error('$.text can only be used as a getter when targeting components');
        }
        
        return $text.apply(this[0].renderedElement);
    }, unsupportedMethod('Components cannot be the target of $.text invocation'));

    $.fn.remove = jQueryPassThrough($remove, function() {
        this[0].detach();
        return this;
    }, function(components, ...args) {
        applyAll(components, ui.Component.prototype.detach);
        return $remove.apply(this, args);
    }); 

    // Components will treat $.detach the same as $.remove since all data *should* be on the Component object
    // But that may change depending on how it's actually used
    $.fn.detach = jQueryPassThrough($detach, function() {
        this[0].detach();
        return this;
    }, function(components, ...args) {
        applyAll(components, ui.Component.prototype.detach);
        return $detach.apply(this, args);
    });

    // Removing all elements from a Component will trigger .detach, so we'll just call that directly
    $.fn.empty = jQueryPassThrough($empty, function() {
        this[0].detach();
        return this;
    });

    // Not going to support $.clone
    $.fn.clone = jQueryPassThrough($clone, unsupportedMethod('$.clone is not supported with components'));

    $.fn.append = jQueryPassThrough($append, function() {
        if(this[0].renderedElement != null && this[0].renderedElement.length) {
            $append.apply($(this[0].renderedElement[this[0].renderedElement.length - 1]), arguments);
        }
        return this;
    }, function(components) {
        applyAll(components, ui.Component.prototype.attach, this);
        applyAll(components, ui.Component.prototype._placeInParent, null, common.placementMethod.append);
        return this;
    });

    $.fn.prepend = jQueryPassThrough($prepend, function() {
        if(this[0].renderedElement != null && this[0].renderedElement.length) {
            $prepend.apply($(this[0].renderedElement[0]), arguments);
        }
        return this;
    }, function(components) {
        applyAll(components, ui.Component.prototype.attach, this);
        applyAll(components, ui.Component.prototype._placeInParent, null, common.placementMethod.prepend);
        return this;
    });

    $.fn.before = jQueryPassThrough($before, function() {
        if(this[0].renderedElement != null && this[0].renderedElement.length) {
            $before.apply($(this[0].renderedElement[0]), arguments);
        }
        return this;
    }, function(components) {
        applyAll(components, ui.Component.prototype.attach, this.parent());
        applyAll(components, ui.Component.prototype._placeInParent, null, common.placementMethod.prepend, this);
        return this;
    });

    $.fn.after = jQueryPassThrough($after, function() {
        if(this[0].renderedElement != null && this[0].renderedElement.length) {
            $after.apply($(this[0].renderedElement[this[0].renderedElement.length - 1]), arguments);
        }
        return this;
    }, function(components) {
        applyAll(components, ui.Component.prototype.attach, this.parent());
        applyAll(components, ui.Component.prototype._placeInParent, null, common.placementMethod.append, this);
        return this;
    });

    $.fn.appendTo = jQueryPassThrough($appendTo, function(target) {
        this[0].attach(target);
        this[0]._placeInParent(null, common.placementMethod.append);
        return this;
    }, function(component) {
        if(component.renderedElement != null && component.renderedElement.length) {
            $appendTo.apply($(component.renderedElement[component.renderedElement.length - 1]), arguments);
        }
        return this;
    });

    $.fn.prependTo = jQueryPassThrough($prependTo, function(target) {
        this[0].attach(target);
        this[0]._placeInParent(null, common.placementMethod.prepend);
        return this;
    }, function(component) {
        if(component.renderedElement != null && component.renderedElement.length) {
            $prependTo.apply($(component.renderedElement[0]), arguments);
        }
        return this;
    });

    $.fn.insertBefore = jQueryPassThrough($insertBefore, function(target) {
        this[0].attach($(target).parent());
        this[0]._placeInParent(null, common.placementMethod.prepend, target);
        return this;
    }, function(component) {
        if(component.renderedElement != null && component.renderedElement.length) {
            $insertBefore.apply($(component.renderedElement[0]), arguments);
        }
        return this;
    });

    $.fn.insertAfter = jQueryPassThrough($insertAfter, function(target) {
        this[0].attach($(target).parent());
        this[0]._placeInParent(null, common.placementMethod.append, target);
        return this;
    }, function(component) {
        if(component.renderedElement != null && component.renderedElement.length) {
            $insertAfter.apply($(component.renderedElement[component.renderedElement.length - 1]), arguments);
        }
        return this;
    });

    $.fn.replaceWith = jQueryPassThrough($replaceWith, function() {
        let ret = this;

        if(this[0].renderedElement != null && this[0].renderedElement.length) {
            ret = $replaceWith.apply($(this[0].renderedElement[0]), arguments);
        }
        this[0].detach();

        return ret;
    }, function(components) {
        applyAll(components, ui.Component.prototype.attach, this.parent());
        applyAll(components, ui.Component.prototype._placeInParent, null, common.placementMethod.append, this);
        return $remove.apply(this);
    });

    $.fn.replaceAll = jQueryPassThrough($replaceWith, unsupportedMethod('Cannot use $.replaceAll with components'), function(component) {
        let ret = this;

        if(component.renderedElement != null && component.renderedElement.length) {
            ret = $replaceWith.apply($(component.renderedElement[0]), arguments);
        }
        component.detach();

        return ret;
    });
}