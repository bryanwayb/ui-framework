'use strict';

const Component = require('./Component.js'),
    common = require('./common.js'),
    objectHash = require('./objectHash.js'),
    Emitter = require('./Emitter.js');

function escapeRegExp(str) {
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}

let routeParameterRegex = /{(.+?)}/g,
    routeQueryStringRegex = /[^{]\?[^}]/;

function extractRouteSections(route) {
    let match = route.match(routeQueryStringRegex),
        queryStringPosition = match ? match.index + 1 : route.length;
    let urlSections = [route.slice(0, queryStringPosition), route.slice(queryStringPosition + 1, route.length)];

    return {
        path: urlSections[0] ? urlSections[0].split('/').splice(1).map(section => {
            return decodeURIComponent(section);
        }) : null,
        query: urlSections[1] ? urlSections[1].split('&').map(section => {
            let keyValueSection = section.split('=');

            return {
                key: decodeURIComponent(keyValueSection[0]),
                value: keyValueSection[1] ? decodeURIComponent(keyValueSection[1]) : true
            };
        }) : null
    };
}

class Router extends Emitter {
    constructor() {
        super();

        this.reset();

        window.addEventListener('popstate', () => {
            this.update();
        }, false);
    }

    reset() {
        this.resolveTree = {};
        this.routeDictionary = {};
        this.parentContainer = null;
        this.paramHash = null;

        if(this.currentNode && this.currentNode.cleanup) {
            this.currentNode.cleanup();
        }

        this.currentNode = null;
    }

    register(routes, component, options = {}) {
        if(typeof routes === 'string') {
            routes = [routes];
        }
        
        if(!process.env.production) {
            let routesValid = false;

            if(routes) {
                if(Array.isArray(routes)) {
                    routesValid = true;
                    for(let i = 0; i < routes.length && routesValid; i++) {
                        routesValid = typeof routes[i] === 'string' && routes[i].indexOf('/') === 0;
                    }
                }
            }

            if(!routesValid) {
                throw new Error(`Invalid parameter for routes has been passed to Router.register: ${routes}.`);
            }

            let extendsComponent = false,
                currentPrototype = component.__proto__;
            while(!extendsComponent && currentPrototype != null) {
                if(currentPrototype === Component) {
                    extendsComponent = true;
                }
                else {
                    currentPrototype = currentPrototype.__proto__;
                }
            }

            if(!extendsComponent) {
                console.warning('A non-Component object has been passed to Router.register.');
            }
        }

        for(let i in routes) {
            let routeObject = extractRouteSections(routes[i]);

            if(!process.env.production) {
                if(!routeObject) {
                    throw new Error(`Error while creating route object for '${routes[i]}'`);
                }
                else if(routeObject.query && routeObject.query.length > 0) {
                    throw new Error(`A query string is not valid as a definition for a route: '${routes[i]}'. Route will never resolve.`);
                }
            }

            let currentResolveNode = this.resolveTree;
            for(let o = 0; o < routeObject.path.length; o++) {
                let nodeValue = routeObject.path[o].toLowerCase(),
                    regexMatch = routeParameterRegex.exec(nodeValue);
                
                if(regexMatch) {
                    let resolvedRegex = '',
                        routeParamNames = [],
                        routeParamSections = [];

                    let lastSpliceIndex = 0,
                        nodeValueSection;
                    do {
                        nodeValueSection = nodeValue.slice(lastSpliceIndex, regexMatch.index);
                        resolvedRegex += escapeRegExp(nodeValueSection);
                        routeParamSections.push(nodeValueSection);

                        let groupId = regexMatch[1],
                            optional = false,
                            optionalIndex = groupId.indexOf('?');

                        if((optional = (optionalIndex !== -1))) {
                            if(!process.env.production) {
                                if(optionalIndex !== groupId.length - 1) {
                                    throw new Error(`Error while parsing route for '${routes[i]}'. '?' is a reserved character and cannot be used in paramter IDs.`);
                                }
                            }

                            groupId = groupId.slice(0, groupId.length - 1);
                        }

                        let groupSections = groupId.split(':'),
                            regexStr = null;
                        
                        groupId = groupSections[0];

                        if(groupSections[1] == null) {
                            groupSections[1] = 'any';
                        }

                        let optionalCharPosition = groupId.indexOf('?'),
                            required;

                        if((required = optionalCharPosition === groupId.length - 1)) {
                            groupId = groupId.substr(0, optionalCharPosition);
                        }

                        switch(groupSections[1]) {
                            case 'number':
                                regexStr = `([\d\.]${required ? '+' : '*'})`;
                            case 'any':
                            case 'string':
                                regexStr = `(.${required ? '+' : '*'})`;
                                break;
                            default:
                                if(!process.env.production) {
                                    throw new Error(`Unknown route data type found for '${routes[i]}': ${groupSections[1]}`);
                                }
                                break;
                        }

                        resolvedRegex += regexStr;

                        if(optional) {
                            resolvedRegex += '?';
                        }

                        routeParamNames.push(groupId);
                        lastSpliceIndex = regexMatch.index + regexMatch[0].length;
                    } while((regexMatch = routeParameterRegex.exec(nodeValue)) != null);

                    nodeValueSection = nodeValue.slice(lastSpliceIndex);
                    routeParamSections.push(nodeValueSection);
                    resolvedRegex += escapeRegExp(nodeValueSection);

                    if(!currentResolveNode._regexNodes) {
                        currentResolveNode._regexNodes = [];
                    }

                    let parent = currentResolveNode;
                    currentResolveNode._regexNodes.push(currentResolveNode = new RegExp(resolvedRegex));
                    currentResolveNode._routeParamNames = routeParamNames;
                    currentResolveNode._routeParamSections = routeParamSections;
                    currentResolveNode._parent = parent;
                }
                else {
                    if(!currentResolveNode[nodeValue]) {
                        currentResolveNode[nodeValue] = {
                            _parent: currentResolveNode,
                            _nodeName: nodeValue
                        };
                    }
                    currentResolveNode = currentResolveNode[nodeValue];
                }
            }

            if(typeof options === 'string'
                || typeof options === 'function') {
                options = {
                    title: options
                };
            }

            let instance = null;
            options.cleanup = () => {
                instance.detach();
                instance = null;
            };

            options.handler = async (params) => {
                instance = new component();
                instance.attach(this.parentContainer);

                if(!process.env.production) {
                    window.currentComponent = instance;
                }

                let title = options.title;

                if(typeof title === 'function') {
                    title = title(params);
                }

                if(title) {
                    document.title = title;
                }

                if(typeof options.renderAsync === 'function') {
                    await options.renderAsync(params, instance.renderAsync.bind(instance));
                }
                else if(typeof options.render === 'function') {
                    await new Promise((resolve) => {
                        if(!process.env.production) {
                            if(instance.async) {
                                console.warn(`Synchronous .render callback used for an asynchronous component. Use the .renderAsync callback instead for execution path integrity.`);
                            }
                        }
                        options.render(params, (instance.async ? instance.renderAsync : instance.render).bind(instance));
                    });
                }
                else {
                    await instance.renderAsync(params);
                }
            };

            options.inlineUpdate = async (params) => {
                if(typeof instance.updateParamsAsync === 'function') {
                    await instance.updateParamsAsync(params);
                }
                else if(typeof instance.updateParams === 'function') {
                    instance.updateParams(params);
                }
            };

            currentResolveNode.options = options;

            if(!this.routeDictionary[component]) {
                this.routeDictionary[component] = [currentResolveNode];
            }
            else {
                this.routeDictionary[component].push(currentResolveNode);
            }
        }

        return component;
    }

    attach(container) {
        this.parentContainer = common.select(container);
        return this.update();
    }

    async update(path = `${window.location.pathname}${window.location.search}`, resolved = this.resolve(path)) {
        if(!process.env.production) {
            if(!resolved) {
                console.error(`Unable to resolve path: '${path}'`);
            }
        }

        let ret = true;

        if(resolved) {
            try {
                let inlineUpdate = false;

                let newHash = objectHash(resolved.params, false, (this.currentNode ? this.currentNode : resolved.node).options.inlineParams);
                if(this.currentNode === resolved.node) {
                    inlineUpdate = newHash === this.paramHash;
                }

                if(inlineUpdate) {
                    await this.currentNode.options.inlineUpdate(resolved.params);
                }
                else {
                    this.paramHash = newHash;

                    if(this.currentNode) {
                        this.currentNode.options.cleanup();
                    }

                    this.currentNode = resolved.node;

                    await this.currentNode.options.handler(resolved.params);
                    await this.currentNode.options.inlineUpdate(resolved.params);
                }

                this.emit('updated', path);
            }
            catch(ex) {
                this.emit('error', ex);

                ret = false;
            }
        }
        else {
            this.emit('unresolved', path);

            ret = false;
        }

        return ret;
    }

    resolve(path) {
        if(!process.env.production) {
            if(path == null) {
                throw new Error('Cannot resolve a null/undefined path');
            }
        }

        let routeSections = extractRouteSections(path),
            currentResolveNode = this.resolveTree,
            params = {};

        for(let i = 0; i < routeSections.path.length; i++) {
            if(currentResolveNode[routeSections.path[i]] != null) {
                currentResolveNode = currentResolveNode[routeSections.path[i]];
            }
            else if(currentResolveNode._regexNodes) {
                let match = null;
                for(let o = 0; o < currentResolveNode._regexNodes.length; o++) {
                    match = currentResolveNode._regexNodes[o].exec(routeSections.path[i]);
                    if(match) {
                        currentResolveNode = currentResolveNode._regexNodes[o];

                        for(let n = 0; n < currentResolveNode._routeParamNames.length; n++) {
                            params[currentResolveNode._routeParamNames[n]] = match[n + 1];
                        }

                        break;
                    }
                }

                if(!match) {
                    return null;
                }
            }
        }

        let ret = null;
        if(currentResolveNode.options) {
            if(routeSections.query && routeSections.query.length) {
                for(let i = 0; i < routeSections.query.length; i++) {
                    params[routeSections.query[i].key] = routeSections.query[i].value;
                }
            }

            ret = {
                node: currentResolveNode,
                params: params
            };
        }

        return ret;
    }

    async link(id, params = {}) {
        if(!process.env.production) {
            if(id == null) {
                throw new Error('A null/undefined id is not allowed');
            }
            else if(this.routeDictionary[id] == null) {
                throw new Error(`Route is not defined for id: '${id}'`);
            }
        }

        let routes = this.routeDictionary[id],
            resolvedRoute = null,
            paramKeys = Object.keys(params),
            missingThreshold = null;

        // TODO: Need to make this find the best possible match based on supplied parameters
        // TODO: Need to look at parent route nodes to find best match
        // TODO: Need to cache the resolved route, somehow
        for(let i = 0; i < routes.length; i++) {
            let missingCount;
            if(routes[i]._routeParamNames) {
                missingCount = 0;
                for(let o = 0; o < routes[i]._routeParamNames.length; o++) {
                    if(paramKeys.indexOf(routes[i]._routeParamNames[o]) === -1) {
                        missingCount++;
                    }
                }
            }
            else {
                missingCount = paramKeys.length;
            }

            if(missingCount < missingThreshold || missingThreshold == null) {
                resolvedRoute = routes[i];
                missingThreshold = missingCount;
            }
        }

        if(!process.env.production) {
            if(!resolvedRoute) {
                throw new Error(`Unable to resolve a route for id: '${id}'`);
            }
        }

        let uri = null;
        if(typeof resolvedRoute.options.preLinkResolve === 'function') {
            uri = resolvedRoute.options.preLinkResolve(params);
        }

        if(typeof resolvedRoute.options.preLinkResolveAsync === 'function') {
            uri = await resolvedRoute.options.preLinkResolveAsync(params);
        }

        if(!uri) {
            let currentNode = resolvedRoute;
        
            let urlSections = [],
                usedParams = {
                    length: 0
                };
            while(currentNode._parent != null) {
                if(currentNode instanceof RegExp) {
                    let sectionValue = '',
                        i = 0;
                    for(; i < currentNode._routeParamNames.length; i++) {
                        if(params[currentNode._routeParamNames[i]]) {
                            sectionValue += `${currentNode._routeParamSections[i]}${params[currentNode._routeParamNames[i]]}`;
                            usedParams[currentNode._routeParamNames[i]] = true;
                            usedParams.length++;
                        }
                    }

                    urlSections.push(encodeURIComponent(`${sectionValue}${currentNode._routeParamSections[i]}`)); // _routeParamSections will always be 1 element longer than _routeParamNames
                }
                else {
                    urlSections.unshift(encodeURIComponent(currentNode._nodeName));
                }

                currentNode = currentNode._parent;
            }

            uri = `/${urlSections.join('/')}`;

            let queryStringSections = [];

            if(usedParams.length < paramKeys.length) {
                for(let i = 0; i < paramKeys.length; i++) {
                    if(!usedParams[paramKeys[i]]) {
                        queryStringSections.push(`${encodeURIComponent(paramKeys[i])}=${encodeURIComponent(params[paramKeys[i]])}`);
                    }
                }

                if(queryStringSections.length > 0) {
                    uri += `?${queryStringSections.join('&')}`;
                }
            }

            if(typeof resolvedRoute.options.postLinkResolve === 'function') {
                uri = resolvedRoute.options.postLinkResolve(params, uri);
            }

            if(typeof resolvedRoute.options.postLinkResolveAsync === 'function') {
                uri = await resolvedRoute.options.postLinkResolve(params, uri);
            }
        }

        return uri;
    }

    async push(id, params = {}) {
        let uri = await this.link(id, params);

        if(this.parentContainer) {
            let resolved = null;
            if(`${window.location.pathname}${window.location.search}` === uri) {
                this.update(uri);
            }
            else if((resolved = this.resolve(uri))) {
                if(this.update(uri, resolved)) {
                    history.pushState(params, '', uri);
                }
            }
            else {
                window.location.href = uri;
            }
        }
        else {
            window.location.href = uri;
        }

        return uri;
    }
}

module.exports = new Router();