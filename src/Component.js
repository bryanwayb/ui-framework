'use strict';

const common = require('./common.js'),
    objectHash = require('./objectHash.js'),
    Emitter = require('./Emitter.js');

const componentMeethodExportsCache = {};

const defaultOptions = {
    events: ['click'],
    renderReuse: false
};

class Component extends Emitter {
    constructor(template, options) {
        super();

        this.template = null;
        this.templatePromise = null;
        this.async = common.isPromise(template);

        if(this.async) {
            this.templatePromise = template;
        }
        else {
            if(!process.env.production) {
                if(typeof template !== 'string' && typeof template !== 'function') {
                    throw new Error('Provided template must be either a string or function for synchronous use, or a Promise for asynchronous use');
                }
            }

            if(typeof template === 'string') {
                this.template = () => {
                    return template;
                };
            }
            else {
                this.template = template;
            }
        }

        if(Array.isArray(options)) {
            options = {
                events: options
            };
        }
        else if(!process.env.production) {
            if(options != null && typeof options !== 'object') {
                throw new Error('Invalid options paramter supplied');
            }
        }

        this.options = Object.assign({}, defaultOptions, options);

        this.renderedElement = null;
        this.parentContainer = null;
        this.listener = null;

        this.refs = null;
        this.data = null;
    }

    static registerElement(tag, component) {
        if(document.registerElement) {
            class ComponentElement extends HTMLElement {
                createdCallback() {
                    if(this.componentInstance != null) {
                        this.componentInstance.detach();
                    }
                    
                    this.componentInstance = new component();
                    this.componentInstance.attach(this).render(this.getAttributeObject());
                }

                getAttributeObject() {
                    let attributes = {};

                    for(let i = 0; i < this.attributes.length; i++) {
                        attributes[this.attributes[i].nodeName] = this.attributes[i].nodeValue;
                    }

                    return attributes;
                }

                attributeChangedCallback() {
                    if(this.componentInstance != null) {
                        this.componentInstance.render(this.getAttributeObject());
                    }
                }

                disconnectedCallback() {
                    if(this.componentInstance != null) {
                        this.componentInstance.detach();
                    }
                }
            }
            document.registerElement(tag, ComponentElement);
        }
    }

    attach(parentContainer) {
        if(!process.env.production) {
            if(parentContainer == null) {
                throw new Error('Tried to attach to a null parent');
            }
        }

        if(this.parentContainer != null) {
            this.detach(false);
        }

        this.parentContainer = common.select(parentContainer);

        if(window.MutationObserver) {
            this.listener = new MutationObserver((mutations) => {
                if(this.renderedElement == null
                    || this.renderedElement.length === 0) {
                    return;
                }

                for(let i = 0; i < mutations.length; i++) {
                    for(let o = 0; o < mutations[i].removedNodes.length; o++) {
                        if(mutations[i].removedNodes[o].parentElement !== this.parentContainer // TODO: This check may not be needed
                            && this.renderedElement.indexOf(mutations[i].removedNodes[o]) !== -1) {
                            this.detach();

                            return;
                        }
                    }
                }
            });

            this.listener.observe(this.parentContainer, {
                childList: true
            });
        }
        else {
            let removing = false;
            this.parentContainer.addEventListener('DOMNodeRemoved', (this.listener = (event) => {
                if(!removing && event.currentTarget === this.parentContainer) {
                    if(this.renderedElement.indexOf(event.target) !== -1) {
                        removing = true;
                        process.nextTick(() => {
                            this.detach();
                        });
                    }
                }
            }), false);
        }

        return this;
    }

    detach(deleteElement = true) {
        let ret = true;

        if(this.renderedElement != null && typeof this.onDetaching === 'function') {
            ret = this.onDetaching();
        }

        if(ret) {
            if(this.listener) {
                if(window.MutationObserver) {
                    this.listener.disconnect();
                }
                else {
                    this.parentContainer.removeEventListener('DOMNodeRemoved', this.listener);
                }
            }
            
            this.remove();

            if(deleteElement) {
                this.renderedElement = null;
            }

            this.parentContainer = null;
        }

        return ret;
    }

    remove() {
        if(this.renderedElement != null) {
            for(let i = this.renderedElement.length - 1; i >= 0; i--) {
                this.renderedElement[i].remove();
            }
        }
    }

    _bind() {
        let exportMethods = componentMeethodExportsCache[this.constructor];
        if(exportMethods == null) {
            let currentPrototype = Object.getPrototypeOf(this),
                lookup = {};

            while(currentPrototype != null && currentPrototype.constructor !== Component) {
                for(var i of Object.getOwnPropertyNames(currentPrototype)) {
                    lookup[i] = true;
                }

                currentPrototype = Object.getPrototypeOf(currentPrototype);
            }

            exportMethods = componentMeethodExportsCache[this.constructor] = Object.keys(lookup);
        }

        let eventArguments = ['event', 'context', ...exportMethods].join(','),
            eventMethods = null;
        
        this.refs = {};

        common.selectAll([ ...(this.options.events
                .map((value) => {
                    return `[on${value}]`;
                })), '[ref]' ]
            .join(','), this.renderedElement).forEach((element) => {
            for(let i = 0; i < this.options.events.length; i++) {
                let eventAttributeName = `on${this.options.events[i]}`,
                    eventAttributeValue = element.getAttribute(eventAttributeName);
                    
                if(eventAttributeValue != null) {
                    let compiledFunction = null,
                        self = this;
                    element.addEventListener(this.options.events[i], (event) => {
                        if(compiledFunction == null) {
                            compiledFunction = eval(`(function(${eventArguments}) {${eventAttributeValue}});`).bind(element);
                        }

                        if(eventMethods == null) {
                            eventMethods = exportMethods.map((name) => {
                                return function() {
                                    self[name].apply(self, arguments);
                                };
                            })
                        }

                        compiledFunction.apply(element, [ event, self, ...eventMethods ]);
                    });

                    element.removeAttribute(eventAttributeName);
                }
            }

            let refAtrributeValue = element.getAttribute('ref');
                if(refAtrributeValue != null) {
                    if(this.refs[refAtrributeValue] != null) {
                        if(!Array.isArray(this.refs[refAtrributeValue])) {
                            this.refs[refAtrributeValue] = [this.refs[refAtrributeValue]];
                        }
                        this.refs[refAtrributeValue].push(element);
                    }
                    else {
                        this.refs[refAtrributeValue] = element;
                    }
                }
                element.removeAttribute('ref');
        }, this);
    }

    _placeInParent(renderedElement, placementMethod = common.placementMethod.append, relativeTo = null) {
        if(renderedElement == null) {
            renderedElement = this.renderedElement;

            if(!process.env.production) {
                if(renderedElement == null) {
                    throw new Error('Component must be rendered before placement');
                }
            }
        }
        else {
            this.renderedElement = common.parseHtml(renderedElement);

            this._bind();
        }

        if(this.parentContainer) {
            if(placementMethod & common.placementMethod.empty) {
                while (this.parentContainer.firstChild) {
                    this.parentContainer.removeChild(this.parentContainer.firstChild);
                }
            }

            if(placementMethod & common.placementMethod.append) {
                if(relativeTo != null) {
                    relativeTo = common.select(relativeTo).nextSibling;

                    for(let i = this.renderedElement.length - 1; i >= 0; i--) {
                        this.parentContainer.insertBefore(this.renderedElement[i], relativeTo);
                        relativeTo = this.renderedElement[i];
                    }
                }
                else {
                    for(let i = 0; i < this.renderedElement.length; i++) {
                        this.parentContainer.appendChild(this.renderedElement[i]);
                    }
                }
            }
            else if(placementMethod & common.placementMethod.prepend) {
                if(relativeTo != null) {
                    relativeTo = common.select(relativeTo);
                }

                for(let i = this.renderedElement.length - 1; i >= 0; i--) {
                    this.parentContainer.insertBefore(this.renderedElement[i], relativeTo ? relativeTo : this.parentContainer.firstChild);
                    relativeTo = this.renderedElement[i];
                }
            }
        }
    }

    _render(data) {
        if(typeof this.onComponentRendering === 'function') {
            let newData = this.onComponentRendering(data);

            if(newData !== undefined) {
                data = newData;
            }
        }

        let shouldRender = !this.options.renderReuse;

        if(!shouldRender) {
            if(data != this.data) {
                let currentDataHash,
                    newDataHash;

                if(this.data != null) {
                    currentDataHash = objectHash(this.data);
                }
                
                if(data != null) {
                    newDataHash = objectHash(data);
                }

                shouldRender = currentDataHash !== newDataHash;
            }
            else {
                shouldRender = objectHash(this.data, true) !== objectHash(this.data, false);
            }
        }

        if(data !== undefined) {
            this.data = data;
        }

        let element = null;
        if(shouldRender || this.renderedElement == null) {
            element = this.template.call(this, this.data);
        }

        let previousElement = this.renderedElement;

        this._placeInParent(element, common.placementMethod.append, this.renderedElement);

        if(previousElement && previousElement !== this.renderedElement) {
            for(let i = 0; i < previousElement.length; i++) {
                this.parentContainer.removeChild(previousElement[i]);
            }
        }

        if(typeof this.onComponentReady === 'function') {
            this.onComponentReady(this.renderedElement);
        }

        return this;
    }

    render() {
        if(!process.env.production) {
            if(this.async) {
                throw new Error('Cannot execute sync .render() method with an async template');
            }
        }
        
        return this._render.apply(this, arguments);
    }

    async renderAsync(data) {
        if(this.async && !this.template) {
            this.template = await this.templatePromise;
        }

        if(typeof this.onComponentRenderingAsync === 'function') {
            let newData = await this.onComponentRenderingAsync(data);

            if(newData !== undefined) {
                data = newData;
            }
        }

        let ret = this._render(data);

        if(typeof this.onComponentReadyAsync === 'function') {
            await this.onComponentReadyAsync(this.renderedElement);
        }

        return ret;
    }
}

module.exports = Component;