'use strict';

let md5 = require('./js-crypto/md5.js');

module.exports = function createHash(obj, useExisting = true, ignoreMap = null) {
    let hash = null;

    if(obj != null) {
        if(useExisting) {
            hash = obj['$$hash'];
        }

        if(!hash) {
            let hashString,
                store = true;

            if(typeof obj === 'string') {
                hashString = obj;
            }
            else if(typeof obj === 'number') {
                hashString = obj.toString();
                store = false;
            }
            else if(typeof obj === 'object') {
                if(Array.isArray(obj)) {
                    let mapped = [];

                    for(let i = 0; i < obj.length; i++) {
                        mapped[i] = createHash(obj[i], useExisting, ignoreMap ? null : ignoreMap[i]);
                    }

                    hashString = `[${mapped.join(',')}]`;
                }
                else {
                    hashString = '';
                    
                    let keys = Object.keys(obj);

                    for(let i = 0; i < keys.length; i++) {
                        if(keys[i] === '$$hash'
                            || (ignoreMap && ignoreMap[keys[i]])) { // Don't hash the hash or any ignored properties
                            continue;
                        }

                        switch(typeof obj[keys[i]]) {
                            case 'string':
                            case 'number':
                                hashString += keys[i] + '=' + obj[keys[i]];
                                break;
                            case 'object':
                                hashString += keys[i] + '=' + createHash(obj[keys[i]], useExisting);
                                break;
                        }

                        if(i !== keys.length - 1) {
                            hashString += ':'
                        }
                    }
                }
            }
            
            if(hashString) {
                hash = md5(hashString);

                if(store) {
                    if(!Object.isFrozen(obj)
                        && (obj.hasOwnProperty('$$hash') || !Object.isSealed(obj))) {
                        obj['$$hash'] = hash;
                    }
                }
            }
        }
    }

    return hash;
};