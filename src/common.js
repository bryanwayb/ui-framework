'use strict';

// IE 11 support
if (!Element.prototype.matches) {
    Element.prototype.matches = Element.prototype.msMatchesSelector;
}

function select(sel, scope, method) {
    let ret = sel;

    if(typeof sel === 'string') {
        if(!Array.isArray(scope)) {
            scope = [scope];
        }

        ret = [];

        for(let i = 0; i < scope.length; i++) {
            if(scope[i].matches != null && scope[i].matches(sel)) {
                ret.push(scope[i]);
            }

            let selectorMethod = scope[i][method];
            if(selectorMethod) {
                let selected = selectorMethod.call(scope[i], sel);

                if(selected != null) {
                    if(selected.length != null) {
                        if(selected.length > 0) {
                            if(!Array.isArray(selected)) {
                                selected = Array.from(selected);
                            }
                            Array.prototype.push.apply(ret, selected);
                        }
                    }
                    else {
                        ret.push(selected);
                    }
                }
            }
        }
    }
    else if(!Array.isArray(sel)) {
        if(sel.length != null) { // Object is a fake array
            ret = Array.from(sel);
        }
        else {
            ret = [sel];
        }
    }

    return ret;
}

module.exports = {
    placementMethod: {
        empty: 1,
        append: 1 << 1,
        prepend: 1 << 2
    },
    parseHtml: (html) => {
        let container = document.createElement('div');
        container.innerHTML = html;
        return Array.from(container.childNodes);
    },
    select: (sel, scope = window.document) => {
        let ret = select(sel, scope, 'querySelector');

        if(ret != null && Array.isArray(ret) && ret.length > 0) {
            ret = ret[0];
        }
        else {
            ret = null;
        }

        return ret;
    },
    selectAll: (sel, scope = window.document) => {
        return select(sel, scope, 'querySelectorAll');
    },
    isPromise: (obj) => {
        return obj && typeof obj.then === 'function';
    }
};