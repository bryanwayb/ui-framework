const ui = require('../../../../dist.js');

class Home extends ui.Component {
    constructor() {
        super(new Promise((resolve, reject) => {
            resolve(require('./home.html'));
        }));

        //super(require('./home.html'));
    }

    testEvent() {
        console.log('testing');
    }

    buttonClick() {
        ui.Router.push(require('../../index.js').components.pages.home, {
            id: 'test',
            asdf: 'this is a test string',
            extra: 'stuff that should be uri encoded'
        });
    }
}

class ExampleElement extends ui.Component {
    constructor() {
        super((data) => `<div>Example element template: ${data['data-text']}</div>`);
    }
}

ui.Component.registerElement('example-element', ExampleElement);

module.exports = Home;