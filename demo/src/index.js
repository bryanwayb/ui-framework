const ui = require('../../dist.js');

module.exports = {
    components: {
        pages: {
            home: ui.Router.register(['/test_{id}_for_{asdf:string?}_sure', '/home/{id}', '/'], require('./components/pages/home.js'), {
                title: (params) => {
                    return `Home: ${params.id ? params.id : ''}`;
                },
                renderAsync: (params, render) => {
                    render(params);
                }
            })
        }
    }
};

window.Router = ui.Router;

window.demo = module.exports;