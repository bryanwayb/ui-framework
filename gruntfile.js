const path = require('path'),
    through = require('through2');

function source() {
    return path.resolve(path.resolve(__dirname, './temp/'), Array.prototype.slice.call(arguments).join());
}

function build() {
    return path.resolve(path.resolve(__dirname, './dist/'), Array.prototype.slice.call(arguments).join());
}

function demoSource() {
    return path.resolve(path.resolve(__dirname, './temp/demo/src/'), Array.prototype.slice.call(arguments).join());
}

function demoBuild() {
    return path.resolve(path.resolve(__dirname, './demo/build/'), Array.prototype.slice.call(arguments).join());
}

const sourceFiles = [
    'index.js',
    'dist.js',
    'src/*.js',
    'src/**/*.js',
    'demo/server.js',
    'demo/index.html',
    'demo/src/*.js',
    'demo/src/**/*.js',
    'demo/src/*.html',
    'demo/src/**/*.html'
];

var SanitizeRegexBackslash = /\\/gm,
    SanitizeRegexBackspace = /\u0008/gm,
    SanitizeRegexTab = /\t/gm,
    SanitizeRegexNewline = /\n/gm,
    SanitizeRegexVerticalTab = /\v/gm,
    SanitizeRegexFeed = /\f/gm,
    SanitizeRegexReturn = /\r/gm,
    SanitizeRegexDoubleQuote = /\"/gm,
    SanitizeRegexQuote = /\'/gm;

function sanitizeRaw(escapedStr) {
    return escapedStr
        .replace(SanitizeRegexBackslash, '\\\\')
        .replace(SanitizeRegexBackspace, '\\b')
        .replace(SanitizeRegexTab, '\\t')
        .replace(SanitizeRegexNewline, '\\n')
        .replace(SanitizeRegexVerticalTab, '\\v')
        .replace(SanitizeRegexFeed, '\\f')
        .replace(SanitizeRegexReturn, '\\r')
        .replace(SanitizeRegexDoubleQuote, '\\\"')
        .replace(SanitizeRegexQuote, '\\\'');
}

module.exports = function (grunt) {
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        env: {
            dist: {
                production: 'production'
            },
            dev: {
                development: 'development'
            }
        },
        babel: {
            options: {
                presets: ['es2015', 'es2017'],
                plugins: ['transform-inline-environment-variables', 'transform-regenerator'],
                sourceMaps: 'inline'
            },
            dist: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        cwd: './',
                        src: [
                            'index.js',
                            'dist.js',
                            'src/{,*/}*.js',
                            'src/**/{,*/}*.js',
                            'demo/{,*/}*.js',
                            'demo/**/{,*/}*.js',
                            source`!${demoBuild`./`}`
                        ],
                        dest: source`./`
                    }
                ]
            }
        },
        copy: {
            dist: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        cwd: './',
                        src: [
                            'src/{,*/}*.html',
                            'src/**/{,*/}*.html',
                            'demo/{,*/}*.html',
                            'demo/**/{,*/}*.html'
                        ],
                        dest: source`./`
                    }
                ]
            },
            dev: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        cwd: './',
                        src: [
                            'src/{,*/}*.js',
                            'src/**/{,*/}*.js',
                            'dist.js',
                            'index.js',
                            'demo/{,*/}*.js',
                            'demo/**/{,*/}*.js'
                        ],
                        dest: source`./`
                    }
                ]
            }
        },
        browserify: {
            dist: {
                files: {
                    [build`index.js`]: [
                        source`dist.js`
                    ]
                }
            },
            demo: {
                files: {
                    [demoBuild`index.js`]: [
                        demoSource`index.js`
                    ]
                }
            },
            options: {
                browserifyOptions: {
                    debug: true,
                    transform: [
                        function (filename) {
                            return through.obj(function (row, encoding, next) {
                                var source = row.toString();
                                if (path.extname(filename).trim().toLowerCase() === '.html') {
                                    source = 'module.exports = ' + require('dot').template(source).toString();
                                }
                                this.push(source);
                                return next();
                            },
                            function (next) {
                                return next();
                            });
                        }
                    ]
                }
            }
        },
        exorcise: {
            dist: {
                files: {
                    [build`./index.js.map`]: [build`index.js`]
                }
            }
        },
        uglify: {
            dist: {
                files: {
                    [build`index.min.js`]: [build`index.js`]
                }
            },
            options: {
                sourceMap: false,
                compress: {
                    properties: true,
                    dead_code: true,
                    drop_debugger: true,
                    conditionals: true,
                    comparisons: true,
                    evaluate: true,
                    booleans: true,
                    loops: true,
                    unused: true,
                    hoist_funs: true,
                    if_return: true,
                    join_vars: true,
                    cascade: true,
                    collapse_vars: true,
                    reduce_vars: true,
                    warnings: true,
                    drop_console: true
                },
                report: 'gzip'
            }
        },
        clean: {
            options: {
                force: true
            },
            temp: {
                src: [
                    source`./`
                ]
            },
            dist: {
                src: [
                    build`./`
                ]
            },
            demo: {
                src: [
                    demoBuild`./`
                ]
            }
        },
        jscs: {
            test: {
                src: sourceFiles,
                options: {
                    config: '.jscsrc',
                    fix: false
                }
            },
            fix: {
                src: sourceFiles,
                options: {
                    config: '.jscsrc',
                    fix: true
                }
            }
        },
        jshint: {
            test: {
                src: sourceFiles,
                options: {
                    jshintrc: '.jshintrc'
                }
            }
        },
        watch: {
            options: {
                spawn: false
            },
            dev: {
                files: sourceFiles,
                tasks: ['dev']
            }
        },
        bgShell: {
            dev: {
                cmd: 'node ./demo/server.js',
                bg: true
            }
        }
    });

    grunt.task.registerTask('default', ['bgShell:dev', 'watch:dev']);

    grunt.task.registerTask('dev', ['clean:dist', 'env:dev', 'copy:dev', 'copy:dist', 'browserify:demo', 'clean:temp']);
    grunt.task.registerTask('debug', ['clean:dist', 'babel:dist', 'copy:dist', 'browserify:demo', 'clean:temp']);
    grunt.task.registerTask('dist', ['clean:dist', 'env:dist', 'babel:dist', 'copy:dist', 'browserify:dist', 'exorcise:dist', 'uglify:dist', 'clean:temp']);

    grunt.task.registerTask('fix', ['jscs:fix']);
    grunt.task.registerTask('test', ['jscs:test', 'jslint:test']);
};