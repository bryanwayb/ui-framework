'use strict';

if(!process.env.development) {
    require("regenerator-runtime/runtime");
    require('core-js/fn/array/from');
    require('core-js/fn/object/assign');
    require('core-js/fn/promise');
    require('core-js/fn/symbol');
}

module.exports = window.ui = require('./index.js');